#!/bin/bash
#Date: 2018-12-01

  # Time Services 
    timedatectl set-timezone America/New_York 
  # Firewall 
    firewall-cmd --add-port=8140/tcp --permanent
    firewall-cmd --reload 

  # Modify hosts 
    192.168.27.195 t0.erebus  # Done 
    192.168.27.230 kwp1.erebus # Done
  
  # Enable NTP 
    yum -y install ntp 
    systemctl enable ntpd 
    systemctl start ntpd 


  # Puppet Repo 
    sudo rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm

  # Install puppetserver 
    yum -y install puppetserver 

  # Sanity Aid 
    echo "alias puppet='/opt/puppetlabs/bin/puppet'" >> ~/.bashrc 
    source ~/.bashrc 

  # Enable Server as a Service 
    systemctl enable puppetserver 
    systemctl start puppetserver 

  # Set Server Name 
    puppet config set server 't0.erebus'

  # [Possibly] Disable SELinux... 

# Puppet Agent 
  
  # Time Services 
    timedatectl set-timezone America/New_York 
  # Firewall 
    firewall-cmd --add-port=8140/tcp --permanent
    firewall-cmd --reload 

  # Modify hosts 
    192.168.27.195 t0.erebus  # Done 
    192.168.27.230 kwp1.erebus # Done

  # Puppet Repo 
    sudo rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm

  # Install puppet-agent 
    yum -y install puppet-agent 
    yum -y install ntp 
    systemctl enable ntpd 
    systemctl start ntpd 

  # Sanity Aid 
    echo "alias puppet='/opt/puppetlabs/bin/puppet'" >> ~/.bashrc 
    source ~/.bashrc 

  # Enable agent service 
    puppet resource service puppet ensure=running enable=true

  # Set Server Name 
    puppet config set server 't0.erebus'

  # Sign Cert 
    puppet agent --test 
    puppet agent --test --waitforcert 10 # Simultaneously sign on master below.  

# On Master 

  # Sign waiting certificate 
    puppet cert sign --all  # Agent should now be signed 


# Manually Install MySQL + Apache Modules to Master 
puppet module install puppetlabs-mysql 
puppet module install puppetlabs-apache

#########################
#   M A N I F E S T S   #
#########################

mkdir -p /etc/puppetlabs/code/environments/production/modules/kwp/manifests 
touch /etc/puppetlabs/code/environments/production/modules/kwp/manifests/init.pp 


# Base 
##############################################################################
/etc/puppetlabs/code/environments/production/modules/kwp/manifests/init.pp
##############################################################################

class kwp {
  include kwp::update
  include kwp::services
  include kwp::database
  include kwp::files
}

# Packages 
##############################################################################
/etc/puppetlabs/code/environments/production/modules/kwp/manifests/update.pp 
##############################################################################

class kwp::update {
  exec { 'yum-update':
    command => '/usr/bin/yum -y update'
  }
  package { 'epel-release': 
    require => Exec['yum-update'],
    ensure  => installed,
  }

  $packages = [ 'mariadb', 'mariadb-server', 'wordpress', 'php-curl', 'php-gd', 'php-mbstring', 'php-xml', 'php-xmlrpc', 'php-soap', 'php-intl', 'php-zip']

  package { $packages: 
    require => Exec['yum-update'],
    ensure  => installed, 
  }
}

# Services
##############################################################################
/etc/puppetlabs/code/environments/production/modules/kwp/manifests/services.pp 
##############################################################################

class kwp::services {
  # MySQL service 
  service { 'mysql':
    name   => 'mariadb'
    enable => true, 
    ensure => running, 
    require => Package["mariadb-server"],
  }
}

# Database
##############################################################################
/etc/puppetlabs/code/environments/production/modules/kwp/manifests/database.pp 
##############################################################################

class kwp::database {
  exec { "set-mysql-password":
    unless => "mysqladmin -uroot -p$mysql_password status",
    path => ["/bin", "/usr/bin"],
    command => "mysqladmin -uroot password $mysql_password",
    require => Service["mariadb"],
  }

  define mysqldb( $user, $password ) {
  exec { "create-wordpress-db":
    unless => "/usr/bin/mysql -u${user} -p${password} wordpress",
    command => "/usr/bin/mysql -uroot -p$mysql_password -e \"create database wordpress; grant all on wordpress.* to ${user}@localhost identified by '$password';\"",
    require => Service["mariadb"],
    }
  }
}

# Configuration Files
##############################################################################
/etc/puppetlabs/code/environments/production/modules/kwp/manifests/files.pp
##############################################################################

# Don't get thrown off by the actual file pathing here -- its arbitrary and specific to puppet.
# The actual path to these configuration files is ../modules/kwp/files/.* 

class kwp::files {
  file { '/var/www/html/wordpress':
  ensure => 'directory',
  }

  file { '/var/www/html/wordpress/wp-config.php':
  ensure => present,
  source => "puppet:///modules/kwp/wp-config.php", 
    }

  file { '/etc/httpd/conf.d/wordpress.conf':
  ensure => present,
  source => "puppet:///modules/kwp/wordpress.conf", 
  }
}

# Modify Primary Site Manifest 
###############################
/etc/puppetlabs/code/environments/production/manifests/site.pp 
###############################
node default { }

node 'kwp1' {
  $mysql_password = "totally_secure"
  $user = 'wordpress'
  $password = '*D65798AAC0E5C6DF3F320F8A30E026E7EBD73A95'
  include kwp

  class { 'apache': 
    default_vhost => false,
    default_mods  => true,
    mpm_module    => prefork, 
  }
  include apache::mod::php
}
